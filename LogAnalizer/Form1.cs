﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogAnalizer
{
    public partial class Form1 : Form
    {
        private SqlConnection sql;

        private ListBox[] boxes;
        private int lines;

        public Form1()
        {
            InitializeComponent();

            boxes = new ListBox[6];

            boxes[0] = listBox2;
            boxes[1] = listBox3;
            boxes[2] = listBox4;
            boxes[3] = listBox5;
            boxes[4] = listBox6;
            boxes[5] = listBox7;
        }

        private void Clear()
        {
            lines = 0;
            listBox1.Items.Clear();

            foreach(ListBox box in boxes)
            {
                box.Items.Clear();
            }
        }

        private void ReadFile(string path)
        {
            if (File.Exists(path) == false)
            {
                Debug.WriteLine("Nie znaleziono pliku");
                return;
            }

            label2.Text = "Czytanie pliku: " + path;

            OpenConnection();

            try
            {
                using (StreamReader sr = File.OpenText(path))
                {
                    string line;

                    while ((line = sr.ReadLine()) != null)
                    {
                        parseLine(line);
                    }

                    label1.Text = "Linii: " + lines.ToString();
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }

            CloseConnection();
        }

        private void parseLine(string line)
        {
            string[] parts = line.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            if (parts.Length != 6)
                return;

            lines++;
            listBox1.Items.Add(line);

            if (InsertData(parts) == false)
                return;

            for (int index = 0; index < parts.Length; index++)
            {
                boxes[index].Items.Add(parts[index]);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string path = textBox1.Text;

            Clear();
            ReadFile(path);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string file = OpenFileDialog();

            if (file != null)
            {
                textBox1.Text = file;
            }
        }

        private void ReadFiles(string path)
        {
            string[] filePaths = getFiles(path);

            if (filePaths == null)
                return;

            int i = 1;
            foreach (string file in filePaths)
            {
                ReadFile(file);

                float progress = i / filePaths.Length * 100;
                progressBar1.Value += (int)progress;

                this.Refresh();
                this.Invalidate();

                progressBar1.Refresh();
                progressBar1.Invalidate();

                i++;
            }
        }

        private string[] getFiles(string path)
        {
            try
            {
                string[] files = Directory.GetFiles(path, "*.txt");
                return files;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            return null;
        }

        private string OpenFileDialog()
        {
            DialogResult result = openFileDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                return openFileDialog1.FileName;
            }

            return null;
        }

        private string OpenDirDialog()
        {
            OpenFileDialog folderBrowser = new OpenFileDialog();

            folderBrowser.ValidateNames = false;
            folderBrowser.CheckFileExists = false;
            folderBrowser.CheckPathExists = true;
            folderBrowser.FileName = "Select folder";

            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                return Path.GetDirectoryName(folderBrowser.FileName);
            }

            return null;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Szymon Nowak oraz Krzysztof Marcinkowski, UZ 2018");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ReadFilesThread();
        }

        private void ReadFilesThread()
        {
            string path = textBox2.Text;

            Clear();
            ReadFiles(path);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string dir = OpenDirDialog();

            if (dir != null)
            {
                textBox2.Text = dir;
            }
        }

        private void OpenConnection()
        {
            string connection = "Data Source=(local);Initial Catalog=HurtowniaLab;Integrated Security=True";
            sql = new SqlConnection(connection);
            sql.Open();
        }

        private void CloseConnection()
        {
            sql.Close();
        }

        private bool InsertData(string[] parts)
        {
            string date = "";
            string time = "";
            try
            {
                date = parts[1].Replace('/', '-');
                time = parts[2].Substring(0, 8);
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.Message);
                Console.WriteLine(e.Message);
                return false;
            }

            string commandText = "Insert into AlarmZoneLog(Zdarzenie, Data, Czas, Source, Destination, Transport) values('" + parts[0] + "','" + date + "','" + time + "','" + parts[3] + "','" + parts[4] + "','" + parts[5] + "');";

            SqlCommand command = new SqlCommand(commandText, sql);

            try
            {
                int result = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.Message);
                Console.WriteLine(e.Message);
                return false;
            }

            return true;
        }
    }
}
